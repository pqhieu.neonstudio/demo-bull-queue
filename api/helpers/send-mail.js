const nodemailer = require('nodemailer');

module.exports = {


  friendlyName: 'Send mail',


  description: '',


  inputs: {
    email: {
      type: 'string',
      required: true,
    },
    otp: {
      type: 'string',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: sails.config.custom.EMAIL,
        pass: sails.config.custom.EMAIL_PASSWORD,
      }
    });
    let mailOptions = {
      form: 'Phan Hieu',
      to: inputs.email,
      subject: 'Authentication Account',
      text: 'OTP code to verify account',
      html: '<p>Your OTP code: <b>' + inputs.otp + '</b></p>',
    }
    transporter.sendMail(mailOptions, function (err, info) {
      if (err) console.log(err);
      else exits.success('Message sent: ' + info.response);
    })
  }
};

