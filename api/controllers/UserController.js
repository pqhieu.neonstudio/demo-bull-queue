const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mongodb = require('mongodb');
const Bull = require('bull');
const queue = new Bull('queue_otp_register');
var admin = require("firebase-admin");
var serviceAccount = require("C:/Users/pqhie/OneDrive/Desktop/sailsjs/auth/auth-test-p-firebase-adminsdk-4ivbj-a5e6dbafd6.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://auth-test-p.firebaseio.com"
});


module.exports = {
  registerView: async function (req, res) {
    return res.view('user/register');
  },

  register: async function (req, res) {

    let phone = '0' + req.body.phone.slice(-9);
    // console.log(phone);
    let user = await sails.getDatastore().manager.collection('user').findOne({ phone: phone });
    if (user !== null) {
      return res.json({ status: 0, statusText: 'phone is exist!!!' });
    }
    // generate otp

    let otp = Math.floor(Math.random() * 9000 + 1000);
    console.log(otp);
    let jobAdd = await queue.add({ phone: phone, otp: otp });
    // let sleep = (ms) => {
    //   return new Promise(resolve => setTimeout(resolve, ms));
    // }
    while (true) {
      let jobWaiting = await queue.getWaiting(0, 0);
      if (jobWaiting[0] === null) {
        continue;
      };
      if (jobWaiting[0].id === jobAdd.id) {
        try {
          let r = await sails.getDatastore().manager.collection('otp_register')
            .findOneAndUpdate(
              { phone: phone },
              { $set: { otp: otp } },
              { upsert: true });

          // let r = await sails.getDatastore().manager.collection('otp_register')
          //   .findOne({ phone: phone });
          // await sails.getDatastore().manager.collection('otp_register')
          //   .updateOne({ phone: phone }, { $set: { otp: otp } });
          // await sails.getDatastore().manager.collection('queue_otp_register')
          //   .insertOne({ otp: r.otp, otp_new: otp });
          
          await jobWaiting[0].remove();
          // await jobWaiting[0].moveToCompleted('completed', false, true);
          break;
        } catch (error) {
          // await jobWaiting[0].moveToFailed({ message: "err"}, true);
          console.log(error);
        }
      }
    }

    // admin.messaging().sendToDevice(
    //   req.body.registrationToken,
    //   {
    //     notification: {
    //       title: 'Verify your phone',
    //       body: 'Your OTP is ' + otp,
    //     }
    //   }
    // )
    //   .then((response) => {
    //     // console.log('successfully: ', response);
    //   })
    //   .catch((error) => {
    //     console.log('error: ', error);
    //   })

    // sign otp_token
    let otp_token = jwt.sign({
      phone: phone
    }, sails.config.custom.SECRET_KEY, { expiresIn: 600 });
    return res.json({ status: 1, otp_token: otp_token });

    // queue.on("global:completed", (jobId) => {
    //   queue.getJob(jobId).then((job)=>{
    //     console.log(job.toJSON());
    //   });
    //   if (jobId === jobAdd.id) {
    //     admin.messaging().sendToDevice(
    //       req.body.registrationToken,
    //       {
    //         notification: {
    //           title: 'Verify your phone',
    //           body: 'Your OTP is ' + otp,
    //         }
    //       }
    //     )
    //       .then((response) => {
    //         // console.log('successfully: ', response);
    //       })
    //       .catch((error) => {
    //         console.log('error: ', error);
    //       })

    //     // sign otp_token
    //     let otp_token = jwt.sign({
    //       phone: phone
    //     }, sails.config.custom.SECRET_KEY, { expiresIn: 600 });
    //     return res.json({ status: 1, otp_token: otp_token });
    //   }
    // })
    // console.log(process.hrtime()[0] + ' ' + process.hrtime()[1] / 1000)
  },

  verifyOtpRegister: async function (req, res) {
    //verify temporary token, if token expired return err
    let otp_register_token = req.body.otp_token;
    try {
      jwt.verify(otp_register_token, sails.config.custom.SECRET_KEY);
    } catch (error) {
      return res.json(error);
    }
    //decode token to get phone
    let otp_payload = jwt.decode(otp_register_token);
    //check otp
    let otp = await sails.getDatastore().manager.collection('otp_register')
      .findOne({ phone: otp_payload.phone });
    if (otp === null || otp.otp != req.body.otp) {
      return res.json('otp incorrect');
    }
    //phone is verified, insert user
    try {
      let r = await sails.getDatastore().manager.collection('user').insertOne({
        phone: otp_payload.phone,
        name: req.body.name,
        email: req.body.email,
        address: req.body.address,
        gender: req.body.gender,
        birthday: new Date(req.body.birthday).getTime(),
        createdAt: Date.now(),
        updatedAt: Date.now(),
      });
      await sails.getDatastore().manager.collection('auth').insertOne({
        user_id: r.insertedId.toString(),
        password: bcrypt.hashSync(req.body.password, 10),
      });
      return res.json('Register success');
    } catch (error) {
      return res.json(error);
    }
  },

  loginView: async function (req, res) {
    return res.view('user/login');
  },

  login: async function (req, res) {
    //find user: User
    let user = await sails.getDatastore().manager.collection('user')
      .findOne({ phone: req.body.phone }, { _id: 1, phone: 1, email: 1 });
    user._id = user._id.toString();
    //check phone
    if (user === null) {
      return res.json({ error: 'Phone or password incorrect' });
    }
    //check password
    let auth = await sails.getDatastore().manager.collection('auth')
      .findOne({ user_id: user._id });
    if (!auth || !bcrypt.compareSync(req.body.password, auth.password)) {
      return res.json({ error: 'Password incorrect' });
    }
    else {
      //if phone and password correct
      //find device:Device
      let userAgent = req.headers['user-agent'];
      let device = await sails.getDatastore().manager.collection('device')
        .findOne({ user_id: user._id, device: userAgent });
      //if device is active, login success
      if (device !== null && device.active === true) {
        let token = await sails.getDatastore().manager.collection('token')
          .findOne({ user_id: user._id, device: userAgent });
        return res.json({ token: token.token, message: 'login success' });
      }

      //if device is not exist, insert device with active false
      if (device === null) {
        try {
          await sails.getDatastore().manager.collection('device')
            .insertOne({ user_id: user._id, device: userAgent, active: false });
        } catch (error) {
          return res.json(error);
        }
      }

      //generate OTP and store
      let otp_code = (Math.floor(Math.random() * 9000) + 1000).toString();
      console.log(otp_code);
      try {
        let otp = await sails.getDatastore().manager.collection('otp')
          .findOneAndUpdate(
            { user_id: user._id, device: userAgent },
            { $set: { otp: otp_code } },
            { upsert: true }
          );
      } catch (error) {
        return res.json(error);
      }

      //send OTP to user email using nodemailer

      let sendMail = await sails.helpers.sendMail.with({ email: user.email, otp: otp_code });
      console.log(sendMail);

      // generate temporary token with user_id, device, otp expires
      let otp_token = jwt.sign({
        user_id: user._id,
        device: userAgent,
      }, sails.config.custom.SECRET_KEY, { expiresIn: 600 });

      // send temporary token for user
      return res.json({ otp_token: otp_token, message: "let's verify otp" });
    }
  },

  verifyOtpView: async function (req, res) {
  },

  verifyOtp: async function (req, res) {
    //verify temporary token, if token expired return err
    let otp_token = req.headers['otp_token']
    try {
      jwt.verify(otp_token, sails.config.custom.SECRET_KEY);
    } catch (error) {
      return res.json(error);
    }
    //decode token to get user_id, device
    let otp_token_payload = jwt.decode(otp_token);
    console.log(otp_token_payload);

    //find otp: Otp
    let otp = await sails.getDatastore().manager.collection('otp')
      .findOne({ user_id: otp_token_payload.user_id, device: otp_token_payload.device });
    //check otp
    if (otp === null || otp.otp !== req.body.otp) {
      return res.json('otp incorrect');
    }
    //update device is active
    try {
      await sails.getDatastore().manager.collection('device').updateOne(
        { user_id: otp_token_payload.user_id, device: otp_token_payload.device },
        { $set: { active: true } }
      );
    } catch (error) {
      return res.json(error);
    }

    //delete old token
    await sails.getDatastore().manager.collection('token')
      .deleteOne({ user_id: otp_token_payload.user_id, device: otp_token_payload.device });

    //generate token and store
    let token_expires = Math.floor(Date.now() / 1000) + 24 * 60 * 60;
    let token = jwt.sign({
      user_id: otp_token_payload.user_id,
      device: otp_token_payload.device,
      exp: token_expires,
    }, sails.config.custom.SECRET_KEY);
    try {
      await sails.getDatastore().manager.collection('token')
        .insertOne({
          user_id: otp_token_payload.user_id,
          token: token,
          device: otp_token_payload.device,
          expires: token_expires
        });
    } catch (error) {
      return res.json(error);
    }
    //send token for user, login success
    return res.json({ token: token, message: 'login success' });
  }
}



// checkPhoneRegister: async function (req, res) {
//   let phone = '0' + req.body.phone.slice(-9);
//   console.log(phone);
//   let user = await sails.getDatastore().manager.collection('user').findOne({ phone: phone });
//   if (user !== null) {
//     return res.json({ status: 'exist' });
//   } else {
//     return res.json({ status: 'not exist' })
//   }
// },

// register: async function (req, res) {
//   console.log(req.body.uid);
//   let userRecord = await (await admin.auth().getUser(req.body.uid)).toJSON();
//   console.log(userRecord);
//   if (userRecord.phoneNumber === req.body.phone) {
//     try {
//       let r = await sails.getDatastore().manager.collection('user').insertOne({
//         phone: '0' + req.body.phone.slice(-9),
//         name: req.body.name,
//         email: req.body.email,
//         address: req.body.address,
//         gender: req.body.gender,
//         birthday: new Date(req.body.birthday).getTime(),
//         createdAt: Date.now(),
//         updatedAt: Date.now(),
//       });
//       await sails.getDatastore().manager.collection('auth').insertOne({
//         user_id: r.insertedId.toString(),
//         password: bcrypt.hashSync(req.body.password, 10),
//       });
//       return res.json('Register success');
//     } catch (error) {
//       return res.json(error);
//     }
//   } else {
//     return res.json('Register error');
//   }

// },

// register not use queue

// register: async function (req, res) {
//   let phone = '0' + req.body.phone.slice(-9);
//   console.log(phone);
//   let user = await sails.getDatastore().manager.collection('user').findOne({ phone: phone });
//   if (user !== null) {
//     return res.json({ status: 0, statusText: 'phone is exist!!!' });
//   }
//   // generate otp

//   let otp = Math.floor(Math.random() * 9000 + 1000);
//   console.log(otp);

//   try {
//     console.log(process.hrtime()[1] / 1000)
//     let r = await sails.getDatastore().manager.collection('otp_register')
//       .findOneAndUpdate(
//         { phone: phone },
//         { $set: { otp: otp } },
//         { upsert: true });
//     console.log(process.hrtime()[1] / 1000)
//     console.log(r.value.otp);
//   } catch (error) {
//     return res.json(error);
//   }

//   // push notification have otp
//   let message = {
//     notification: {
//       title: 'Verify your phone',
//       body: 'Your OTP is ' + otp,
//     }
//   }

//   admin.messaging().sendToDevice(req.body.registrationToken, message)
//     .then((response) => {
//       // console.log('successfully: ', response);
//     })
//     .catch((error) => {
//       console.log('error: ', error);
//     })

//   // sign otp_token
//   let otp_token = jwt.sign({
//     phone: phone
//   }, sails.config.custom.SECRET_KEY, { expiresIn: 600 });
//   return res.json({ status: 1, otp_token: otp_token });
// },

// using collection

// register: async function (req, res) {
//   const client = new mongodb.MongoClient('mongodb://localhost:27017/', { useNewUrlParser: true });
//   try { await client.connect(); } catch (error) { console.log(error); }
//   const db = client.db('auth_user');
//   const queue_otp_register = db.collection('queue_otp_register');

//   let phone = '0' + req.body.phone.slice(-9);
//   console.log(phone);
//   let user = await sails.getDatastore().manager.collection('user').findOne({ phone: phone });
//   if (user !== null) {
//     return res.json({ status: 0, statusText: 'phone is exist!!!' });
//   }
//   // generate otp

//   let otp = Math.floor(Math.random() * 9000 + 1000);
//   // console.log(otp);
//   // console.log(process.hrtime()[0] + ' ' + process.hrtime()[1] / 1000);

//   let addResult = await queue_otp_register.insertOne({
//     createdOn: new Date(),
//     payload: {
//       phone: phone,
//       otp, otp
//     }
//   });
//   let insertedId = addResult.insertedId.toString();

//   let getResult;
//   let flag = true;
//   setTimeout(() => { flag = false; }, 10000);

//   while (flag) {
//     getResult = await queue_otp_register.findOne({}, { $sort: { createdOn: 1 } });
//     // console.log(process.hrtime()[0] + ' ' + process.hrtime()[1] / 1000);
//     // console.log(getResult);
//     if (getResult._id.toString() === insertedId) {
//       await sails.getDatastore().manager.collection('otp_register')
//         .findOneAndUpdate(
//           { phone: getResult.payload.phone },
//           { $set: { otp: getResult.payload.otp } },
//           { upsert: true });

//       await queue_otp_register.deleteOne({ _id: ObjectID(insertedId) });
//       break;
//     }
//   }
//   if (flag === false) {
//     console.log('time up');
//     return res.json('error');
//   }

//   // console.log(process.hrtime()[0] + ' ' + process.hrtime()[1] / 1000)

//   // push notification have otp
//   let message = {
//     notification: {
//       title: 'Verify your phone',
//       body: 'Your OTP is ' + otp,
//     }
//   }

//   admin.messaging().sendToDevice(req.body.registrationToken, message)
//     .then((response) => {
//       // console.log('successfully: ', response);
//     })
//     .catch((error) => {
//       console.log('error: ', error);
//     })

//   // sign otp_token
//   let otp_token = jwt.sign({
//     phone: phone
//   }, sails.config.custom.SECRET_KEY, { expiresIn: 600 });
//   return res.json({ status: 1, otp_token: otp_token });
// },