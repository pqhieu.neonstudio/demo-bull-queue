module.exports = {

  attributes: {
    phone: { type: 'string', required: true, unique: true },
    name: { type: 'string' },
    email: { type: 'string' },
    address: { type: 'string' },
    birthday: { type: 'number' },
    gender: { type: 'string' }
  },

};