module.exports = {
  attributes: {
    user_id: { type: 'string', required: true },
    otp: { type: 'string', required: true },
    device: { type: 'string', required: true },
    // expires: { type: 'number', required: true },
  }
}