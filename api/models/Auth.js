module.exports = {
  attributes: {
    user_id: { type: 'string', required: true },
    password: { type: 'string', required: true },
  }
}