module.exports = {
  attributes: {
    user_id: { type: 'string', required: true },
    token: { type: 'string', required: true },
    device: { type: 'string', required: true },
    expires: { type: 'number', required: true },
  }
}