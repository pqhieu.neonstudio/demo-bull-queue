module.exports = {
  attributes: {
    user_id: { type: 'string', required: true },
    device: { type: 'string', required: true },
    active: { type: 'boolean', required: true },
  }
}