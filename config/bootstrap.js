const Bull = require('bull');
/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function () {

  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return;
  // }
  //
  // await User.createEach([
  //   { emailAddress: 'ry@example.com', fullName: 'Ryan Dahl', },
  //   { emailAddress: 'rachael@example.com', fullName: 'Rachael Shaw', },
  //   // etc.
  // ]);
  // ```
  // let sleep = (ms) => {
  //   return new Promise(resolve => setTimeout(resolve, ms));
  // }
  // const queue = new Bull('queue_otp_register');
  // queue.process(async (job) => {
  //   try {
  //     console.log(process.hrtime()[0] + ' ' + process.hrtime()[1] / 1000);
  //     await sleep(2000);
  //     let r = await sails.getDatastore().manager.collection('otp_register')
  //     .findOneAndUpdate(
  //       { phone: job.data.phone },
  //       { $set: { otp: job.data.otp } },
  //       { upsert: true });
  //     console.log(process.hrtime()[0] + ' ' + process.hrtime()[1] / 1000);
  //     console.log("done: "+ job.id);
  //     console.log(r.value.otp);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // })
};
